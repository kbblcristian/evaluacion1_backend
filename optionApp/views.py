from django.shortcuts import render
from django.http import HttpResponse
import random
# Create your views here.

def vistaTabla(request):

    titulo = "<h1>Mis Países Favoritos</h1>"
    lista = '''
        <table border="1">
            <tr>
                <th>País</th>
                <th>Continente</th>
                <th>Bandera</th>
            </tr>          
            <tr>
                <td>Chile</td>          
                <td>América</td>          
                <td><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Flag_of_Chile.svg/1280px-Flag_of_Chile.svg.png" width=100 /></td>
            </tr>         
            <tr>
                <td>España</td>          
                <td>Europa</td>          
                <td><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Bandera_de_Espa%C3%B1a.svg/1280px-Bandera_de_Espa%C3%B1a.svg.png" width=100 /></td> 
            </tr>          
            <tr>
                <td>Japón</td>          
                <td>Asia</td>          
                <td><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Flag_of_Japan.svg/1280px-Flag_of_Japan.svg.png" width=100 /></td>
            </tr>
        <table>
        <br>
        '''
    volver = "<a href='/'>Volver</a>"
    all = titulo + lista + volver
    return HttpResponse(all)

def vistaLoteria(request):

    titulo = "<h1>Estos son los números de la suerte en este momento</h1>"
    numeros_random = sorted(random.sample(range(1, 101), 10))
    
    num_list = "<ul>" 
    for num in numeros_random:
        num_list +=f"<li>{num}</li>"
    num_list += "</ul>" 

    volver = "<a href='/'>Volver</a>"
    all = titulo + num_list + volver
    return HttpResponse(all)



