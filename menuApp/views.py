from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def vistaMenu(request):
    titulo = "<h1>Seleccione una opción</h1>"
    lista = '''
        <ul>
            <li><a href="option/tabla/">Tabla</a></li>
            <li><a href="option/loteria/">Lotería</a></li>
        </ul>
        '''
    all = titulo + lista
    return HttpResponse(all)